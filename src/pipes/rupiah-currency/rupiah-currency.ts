import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'rupiahCurrency',
})
export class RupiahCurrencyPipe implements PipeTransform {
	transform(value: number | string): string {
		let num = new Intl.NumberFormat('id-ID', {
			minimumFractionDigits: 0
		}).format(Number(value));
		
		return 'Rp. '+num+',-';
	}
}