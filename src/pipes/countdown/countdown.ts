import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'countdown',
})
export class CountdownPipe implements PipeTransform {
	transform(value: string, exponent: string): string {
		var
		release	= new Date(value).getTime(),
		diff	= release - parseFloat(exponent),
		days	= Math.floor(diff / (1000 * 60 * 60 * 24)),
		hours	= Math.floor((diff % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)),
		minutes	= Math.floor((diff % (1000 * 60 * 60)) / (1000 * 60)),
		seconds	= Math.floor((diff % (1000 * 60)) / 1000);
		
		if(days == 0) {
			return '<b>Available Now</b>';
		} else if(days == 1) {
			return '<i>Available In</i><br/>' + this.pad(hours) + ':' + this.pad(minutes) + ':' + this.pad(seconds);
		} else if(days > 0) {
			return '<i>Available In</i><br/>' + (days-1) + 'D ' + this.pad(hours) + ':' + this.pad(minutes) + ':' + this.pad(seconds);
		} else {
			return diff+' - Expired';
		}
	}
	
	pad(num) {
	    var s = num+"";
	    while (s.length < 2) s = "0" + s;
	    return s;
	}
}