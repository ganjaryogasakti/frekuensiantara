import { NgModule } from '@angular/core';
import { SafePipe } from './safe/safe';
import { RupiahCurrencyPipe } from './rupiah-currency/rupiah-currency';
import { CountdownPipe } from './countdown/countdown';
@NgModule({
	declarations: [SafePipe, RupiahCurrencyPipe, CountdownPipe],
	imports: [],
	exports: [SafePipe, RupiahCurrencyPipe, CountdownPipe]
})
export class PipesModule {}
