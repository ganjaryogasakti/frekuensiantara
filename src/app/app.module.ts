import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { IonicStorageModule } from '@ionic/storage';
import { GoogleMaps } from '@ionic-native/google-maps';
import { SocialSharing } from '@ionic-native/social-sharing';

import { MyApp } from './app.component';
import { Tabs } from '../pages/tabs/tabs';
import { News } from '../pages/news/news';
import { Events } from '../pages/events/events';
import { Projekts } from '../pages/projekts/projekts';
import { Vaults } from '../pages/vaults/vaults';
import { Artists } from '../pages/artists/artists';
import { Shop } from '../pages/shop/shop';
import { Post } from '../pages/post/post';
import { EventPost } from '../pages/event/event';
import { Projekt } from '../pages/projekt/projekt';
import { Vault } from '../pages/vault/vault';
import { Artist } from '../pages/artist/artist';
import { Product } from '../pages/product/product';
import { Cart } from '../pages/cart/cart';
import { Checkout } from '../pages/checkout/checkout';
import { Comments } from '../pages/comments/comments';
import { Bookmarks } from '../pages/bookmarks/bookmarks';
import { Search } from '../pages/search/search';
import { EventFilter } from '../pages/event-filter/event-filter';
import { ProjektFilter } from '../pages/projekt-filter/projekt-filter';
import { ArtistFilter } from '../pages/artist-filter/artist-filter';
import { About } from '../pages/about/about';
import { Single } from '../pages/single/single';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { PipesModule } from '../pipes/pipes.module';
import { ApiProvider } from '../providers/api/api';
import { CartProvider } from '../providers/cart/cart';

@NgModule({
	declarations: [
		MyApp,
		Tabs,
		News,
		Events,
		Projekts,
		Vaults,
		Artists,
		Shop,
		Post,
		EventPost,
		Projekt,
		Vault,
		Artist,
		Product,
		Cart,
		Checkout,
		Comments,
		Bookmarks,
		Search,
		EventFilter,
		ProjektFilter,
		ArtistFilter,
		About,
		Single
	],
	imports: [
		BrowserModule,
		HttpModule,
		HttpClientModule,
		PipesModule,
		LazyLoadImageModule,
		IonicStorageModule.forRoot(),
		IonicModule.forRoot(MyApp)
	],
	bootstrap: [IonicApp],
	entryComponents: [
		MyApp,
		Tabs,
		News,
		Events,
		Projekts,
		Vaults,
		Artists,
		Shop,
		Post,
		EventPost,
		Projekt,
		Vault,
		Artist,
		Product,
		Cart,
		Checkout,
		Comments,
		Bookmarks,
		Search,
		EventFilter,
		ProjektFilter,
		ArtistFilter,
		About,
		Single
	],
	providers: [
		StatusBar,
		SplashScreen,
		ApiProvider,
		CartProvider,
		GoogleMaps,
		SocialSharing,
		{provide: ErrorHandler, useClass: IonicErrorHandler}
	]
})
export class AppModule {}
