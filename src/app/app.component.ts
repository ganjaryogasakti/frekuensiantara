import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';

import { Tabs } from '../pages/tabs/tabs';
import { Bookmarks } from '../pages/bookmarks/bookmarks';
import { About } from '../pages/about/about';
import { Single } from '../pages/single/single';

@Component({
	templateUrl: 'app.html'
})
export class MyApp {
	@ViewChild(Nav) nav: Nav;
	onLoading: boolean = false;
	showSplash: boolean = true;
	rootPage: any = Tabs;
	pages: Array<{title: string, component: any, id: number}>;
	loader: any = '../../assets/imgs/splashscreen.svg';
	appLoaded: boolean = false;
	showAudio: boolean = false;
	audioPlayer: any;
	tabShow: boolean = true;

	constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public storage: Storage, public events: Events) {
		this.storage.set('bookmarkPosts', []);
		this.storage.set('cartItems', []);
		this.initializeApp();

		this.pages = [
			{ title: 'Home', component: Tabs, id: 0 },
			{ title: 'Bookmark', component: Bookmarks, id: 0 },
			{ title: 'About', component: About, id: 0 },
			{ title: 'Privacy Policy', component: Single, id: 1054 },
			{ title: 'Terms & Conditions', component: Single, id: 1056 },
			{ title: 'Log In', component: Tabs, id: 0 }
		];
	}

	initializeApp() {
		this.platform.ready().then(() => {
			this.statusBar.styleLightContent();
			this.splashScreen.hide();
			this.appLoaded = true;
			
			setTimeout(() => {
				this.onLoading = true;
				
				setTimeout(() => {
					this.showSplash = false;
				}, 1000);
			}, 2000);
		});
		
		this.events.subscribe('tabs:show', (status) => {
			console.log(status);
			this.tabShow = status;
		});
		
		this.events.subscribe('player:toggle', (open, audio) => {
			this.showAudio = open;
			this.audioPlayer = audio;
		});
	}

	openPage(page) {
		this.nav.setRoot(page.component, {
			id: page.id
		});
	}
	
	closePlayer() {
		this.showAudio = false;
		this.audioPlayer = '';
	}
}
