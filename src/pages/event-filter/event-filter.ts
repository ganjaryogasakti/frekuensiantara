import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

import { ApiProvider } from './../../providers/api/api';

@Component({
	selector: 'page-event-filter',
	templateUrl: 'event-filter.html'
})
export class EventFilter {
	constructor(public viewCtrl: ViewController, public navCtrl: NavController, public navParams: NavParams, public api: ApiProvider) {
	
	};
	
	dismiss() {
		this.viewCtrl.dismiss();
	}
}