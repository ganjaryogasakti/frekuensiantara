import { Component, ElementRef, Renderer } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';

import { ApiProvider } from './../../providers/api/api';

@Component({
	selector: 'page-projekt',
	templateUrl: 'projekt.html'
})
export class Projekt {
	id: number;
	projekt: {};
	loading:	 any;
	pageLoaded: boolean = false;
	scrollerHandle: any;
	scrollTop: any;
	header: any;
	translateAmt: any;
	placeholder: any = '../../assets/imgs/placeholder-article.svg';
	
	constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public api: ApiProvider, public element: ElementRef, public renderer: Renderer) {
		this.id = navParams.get('id');
		this.loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: `<div class="fa-loading"><img src="../../assets/imgs/loader.svg"></div>`
		});
		this.loading.present();
		
		this.api.getProjekt(this.id).then(data => {
			if(data) {
				this.projekt = data;
			} else {
				let toast = this.toastCtrl.create({
					message: 'The projekt is failed to load.',
					duration: 3000,
					position: 'bottom'
				});
				toast.present();
			};
			this.loading.dismiss();
			this.pageLoaded = true;
		});
	};
	
	// Enter Page
	ionViewWillEnter() {
		this.scrollerHandle = this.element.nativeElement.getElementsByClassName('scroll-content')[0];
		this.header = this.scrollerHandle.getElementsByClassName('parallax-bg');
		
		this.scrollerHandle.addEventListener('scroll', () => {
			this.parallax();
		});
	};
	
	// Parallax
	parallax() {
		this.scrollTop = this.scrollerHandle.scrollTop;
		
	    if(this.scrollTop >= 0){
	    	this.translateAmt = this.scrollTop / 2;
	    } else {
	    	this.translateAmt = 0;
	    }
		
		this.renderer.setElementStyle(this.header[0], 'webkitTransform', 'translate3d(0,'+this.translateAmt+'px,0)');
	};
}