import { Component, ElementRef, Renderer } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, Events } from 'ionic-angular';

import { SocialSharing } from '@ionic-native/social-sharing';
import { ApiProvider } from './../../providers/api/api';
import { Comments } from '../comments/comments';

@Component({
	selector: 'page-post',
	templateUrl: 'post.html'
})
export class Post {
	id: number;
	post: {};
	loading:	 any;
	format: any;
	pageLoaded: boolean = false;
	scrollerHandle: any;
	scrollTop: any;
	header: any;
	translateAmt: any;
	isBookmark: boolean = false;
	placeholder: any = '../../assets/imgs/placeholder-article.svg';
	related: any[];

	constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public api: ApiProvider, public element: ElementRef, public renderer: Renderer, private socialSharing: SocialSharing, public events: Events) {
		this.id = navParams.get('id');
		this.loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: `<div class="fa-loading"><img src="../../assets/imgs/loader.svg"></div>`
		});
		this.loading.present();

		this.api.getPost(this.id).then(data => {
			if(data) {
				this.post = data;
				this.format = data['format'];
				this.related = data['related'];
				this.getBookmark(data);
			} else {
				let toast = this.toastCtrl.create({
					message: 'The post is failed to load.',
					duration: 3000,
					position: 'bottom'
				});
				toast.present();
			};
			this.loading.dismiss();
			this.pageLoaded = true;
		});
	};

	// Enter Page
	ionViewWillEnter() {
		this.events.publish('tabs:show', false);
		this.scrollerHandle = this.element.nativeElement.getElementsByClassName('scroll-content')[0];
		this.header = this.scrollerHandle.getElementsByClassName('parallax-bg');

		this.scrollerHandle.addEventListener('scroll', () => {
			if(this.format != 'video') {
				this.parallax();
			};
		});
	};
	
	// Leave Page
	ionViewWillLeave() {
		this.events.publish('tabs:show', true);
	};

	// Parallax
	parallax() {
		this.scrollTop = this.scrollerHandle.scrollTop;

	    if(this.scrollTop >= 0){
	    	this.translateAmt = this.scrollTop / 2;
	    } else {
	    	this.translateAmt = 0;
	    }

		this.renderer.setElementStyle(this.header[0], 'webkitTransform', 'translate3d(0,'+this.translateAmt+'px,0)');
	};

	// Go To Post
	commentsTapped(event, item) {
		this.navCtrl.push(Comments, {
			id: item
		});
	};
	
	// Share Post
	sharePost(event, title, url) {
		this.socialSharing.share(title, '', '', url);
	};
	
	// Get Bookmark
	getBookmark(post) {
		this.api.getBookmark(post).then(data => {
			if(data) {
				this.isBookmark = true;
			};
		});
	};
	
	// Add Bookmark
	addBookmark(event, post) {
		this.api.addBookmark(post).then(data => {
			this.isBookmark = true;
			let toast = this.toastCtrl.create({
				message: 'The post was bookmarked.',
				duration: 3000,
				position: 'bottom'
			});
			toast.present();
		});
	};
	
	// Remove Bookmark
	removeBookmark(event, post) {
		this.api.removeBookmark(post).then(data => {
			this.isBookmark = false;
			let toast = this.toastCtrl.create({
				message: 'The post was remove from your bookmarks.',
				duration: 3000,
				position: 'bottom'
			});
			toast.present();
		});
	};
}
