import { Component, ElementRef } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, ModalController } from 'ionic-angular';

import { ApiProvider } from './../../providers/api/api';
import { EventPost } from '../event/event';
import { EventFilter } from '../event-filter/event-filter';

@Component({
	selector: 'page-events',
	templateUrl: 'events.html'
})
export class Events {
	events: Array<any> = new Array<any>();
	loading:	 any;
	scrollerHandle: any;
	page: number = 1;
	pageLoaded: boolean = false;
	noMoreItemsAvailable: boolean = false;
	placeholder: any = '../../assets/imgs/placeholder-event.svg';

	constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public modalCtrl: ModalController, public api: ApiProvider, public el: ElementRef) {
		this.loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: `<div class="fa-loading"><img src="../../assets/imgs/loader.svg"></div>`
		});
		this.loading.present();
		this.loadEvents();
	};

	// Enter Page
	ionViewWillEnter() {
		this.scrollerHandle = this.el.nativeElement.getElementsByClassName('scroll-content')[0];
	};

	// Load Posts
	loadEvents(infiniteScroll?) {
		this.api.getEvents(this.page).then(data => {
			if(data) {
				this.page++;
				this.events	= this.events.concat(data);
				if(infiniteScroll) {
					infiniteScroll.complete();
				};
				if(!this.pageLoaded) {
					this.loading.dismiss();
					this.pageLoaded = true;
				};
			} else {
				let toast = this.toastCtrl.create({
					message: 'You have reached the last event!',
					duration: 3000,
					position: 'bottom'
				});
				toast.present();
				infiniteScroll.complete();
				infiniteScroll.enable(false);
				this.noMoreItemsAvailable = true;
			};
		});
	};

	// Infinite Scroll
	doInfinite(infiniteScroll) {
		this.loadEvents(infiniteScroll);
	};

	// Go To Post
	eventTapped(event, item) {
		this.navCtrl.push(EventPost, {
			id: item
		});
	};
	
	// Open Filter
	openFilter() {
		const modal = this.modalCtrl.create(EventFilter);
		modal.present();
	};
}
