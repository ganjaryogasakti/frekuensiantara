import { Component, ElementRef } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, ModalController } from 'ionic-angular';

import { ApiProvider } from './../../providers/api/api';
import { Projekt } from '../projekt/projekt';
import { ProjektFilter } from '../projekt-filter/projekt-filter';

@Component({
	selector: 'page-projekts',
	templateUrl: 'projekts.html'
})
export class Projekts {
	projekts: Array<any> = new Array<any>();
	loading:	 any;
	scrollerHandle: any;
	page: number = 1;
	pageLoaded: boolean = false;
	noMoreItemsAvailable: boolean = false;
	placeholder: any = '../../assets/imgs/placeholder-projekt.svg';
	
	constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public modalCtrl: ModalController, public api: ApiProvider, public el: ElementRef) {
		this.loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: `<div class="fa-loading"><img src="../../assets/imgs/loader.svg"></div>`
		});
		this.loading.present();
		this.loadProjekts();
	};
	
	// Enter Page
	ionViewWillEnter() {
		this.scrollerHandle = this.el.nativeElement.getElementsByClassName('scroll-content')[0];
	};
	
	// Load Posts
	loadProjekts(infiniteScroll?) {
		this.api.getProjekts(this.page).then(data => {
			if(data) {
				this.page++;
				this.projekts = this.projekts.concat(data);
				if(infiniteScroll) {
					infiniteScroll.complete();
				};
				if(this.projekts.length < 12) {
					this.noMoreItemsAvailable = true;
				};
				if(!this.pageLoaded) {
					this.loading.dismiss();
					this.pageLoaded = true;
				};
			} else {
				let toast = this.toastCtrl.create({
					message: 'You have already reach the last projekt!',
					duration: 3000,
					position: 'bottom'
				});
				toast.present();
				infiniteScroll.complete();
				infiniteScroll.enable(false);
				this.noMoreItemsAvailable = true;
			};
		});
	};
	
	// Infinite Scroll
	doInfinite(infiniteScroll) {
		this.loadProjekts(infiniteScroll);
	};
	
	// Go To Post
	projektTapped(event, item) {
		this.navCtrl.push(Projekt, {
			id: item
		});
	};
	
	// Open Filter
	openFilter() {
		const modal = this.modalCtrl.create(ProjektFilter);
		modal.present();
	};
}
