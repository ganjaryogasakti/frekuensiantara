import { Component, ElementRef, Renderer } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';

import { ApiProvider } from './../../providers/api/api';

@Component({
	selector: 'page-comments',
	templateUrl: 'comments.html'
})
export class Comments {
	id: number;
	scrollerHandle: any;
	comments: Array<any> = new Array<any>();
	loading: any;
	page: number = 1;
	pageLoaded: boolean = false;
	noMoreItemsAvailable: boolean = false;

	constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public api: ApiProvider, public el: ElementRef, public renderer: Renderer) {
		this.id = navParams.get('id');
		this.loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: '<div class="fa-loading"><img src="../../assets/imgs/loader.svg"></div>'
		});
		this.loading.present();
		this.loadComments();
	};

	// Enter Page
	ionViewWillEnter() {
		this.scrollerHandle = this.el.nativeElement.getElementsByClassName('scroll-content')[0];
	}

	// Load Comments
	loadComments(infiniteScroll?) {
		this.api.getComments(this.id).then(data => {
			console.log(data);
			if(data) {
				this.page++;
				this.comments = this.comments.concat(data);
				if(infiniteScroll) {
					infiniteScroll.complete();
				};
				if(!this.pageLoaded) {
					this.loading.dismiss();
					this.pageLoaded = true;
				};
			} else {
				let toast = this.toastCtrl.create({
					message: 'Last comment reached!',
					duration: 3000,
					position: 'bottom'
				});
				toast.present();
				infiniteScroll.complete();
				infiniteScroll.enable(false);
				this.noMoreItemsAvailable = true;
			};
		});
	};

	// Infinite Scroll
	doInfinite(infiniteScroll) {
		this.loadComments(infiniteScroll);
	};
}
