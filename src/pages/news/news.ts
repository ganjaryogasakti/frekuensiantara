import { Component, ElementRef, ViewChild } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, Events } from 'ionic-angular';

import { ApiProvider } from './../../providers/api/api';
import { Post } from '../post/post';
import { Search } from '../search/search';

@Component({
	selector: 'page-news',
	templateUrl: 'news.html'
})
export class News {
	@ViewChild('searchBar') search : any;
	features: any[];
	posts: any[];
	exclude: any;
	loading:	 any;
	scrollerHandle: any;
	isSearchBarOpen: boolean = false;
	page: number = 1;
	pageLoaded: boolean = false;
	noMoreItemsAvailable: boolean = false;
	placeholder: any = '../../assets/imgs/placeholder-article.svg';

	constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public api: ApiProvider, public el: ElementRef, public events: Events) {
		this.loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: `<div class="fa-loading"><img src="../../assets/imgs/loader.svg"></div>`
		});
		//this.loading.present();

		this.api.getFeatures().then(data => {
			this.page++;
			this.features	= data['features'];
			this.posts		= data['posts'];
			this.exclude		= data['exclude'];
			//this.loading.dismiss();
			this.pageLoaded = true;
		});
	};

	// Enter Page
	ionViewWillEnter() {
		this.scrollerHandle = this.el.nativeElement.getElementsByClassName('scroll-content')[0];
	};

	// Load Posts
	loadPosts(infiniteScroll?) {
		this.api.getPosts(this.page, this.exclude).then(data => {
			if(data) {
				this.page++;
				this.posts = this.posts.concat(data);
				if(infiniteScroll) {
					infiniteScroll.complete();
				};
			} else {
				let toast = this.toastCtrl.create({
					message: 'You have reached the last post!',
					duration: 3000,
					position: 'bottom'
				});
				toast.present();
				infiniteScroll.complete();
				infiniteScroll.enable(false);
				this.noMoreItemsAvailable = true;
			};
		});
	};

	// Infinite Scroll
	doInfinite(infiniteScroll) {
		this.loadPosts(infiniteScroll);
	};

	// Go To Post
	postTapped(event, post) {
		if(post.format != 'audio') {
			this.navCtrl.push(Post, {
				id: post.id
			});
		} else {
			this.events.publish('player:toggle', true, post.audio);
		};
	};

	// Search
	onSearch(event) {
		this.navCtrl.push(Search, {
			key: event.target.value
		});
	};

	// Open Searchbar
	openSearchbar(event) {
		this.isSearchBarOpen = true;
		setTimeout(() => {
			this.search.setFocus();
		}, 500);
		//this.el.nativeElement.getElementsByClassName('searchbar').focus();
	};
}
