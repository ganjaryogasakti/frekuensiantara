import { Component, ElementRef } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';

import { ApiProvider } from './../../providers/api/api';
import { Post } from '../post/post';

@Component({
	selector: 'page-bookmarks',
	templateUrl: 'bookmarks.html'
})
export class Bookmarks {
	posts: Array<any> = new Array<any>();
	loading:	 any;
	scrollerHandle: any;
	pageLoaded: boolean = false;
	placeholder: any = '../../assets/imgs/placeholder-article.svg';

	constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public api: ApiProvider, public el: ElementRef) {
		this.loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: `<div class="fa-loading"><img src="../../assets/imgs/loader.svg"></div>`
		});
		this.loading.present();

		this.api.getBookmarks().then(data => {
			this.posts		= data;
			this.loading.dismiss();
			this.pageLoaded = true;
		});
	};

	// Enter Page
	ionViewWillEnter() {
		this.scrollerHandle = this.el.nativeElement.getElementsByClassName('scroll-content')[0];
	};

	// Go To Post
	postTapped(event, item) {
		this.navCtrl.push(Post, {
			id: item
		});
	};
}
