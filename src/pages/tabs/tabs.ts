import { Component } from '@angular/core';

import { News } from '../news/news';
import { Events } from '../events/events';
import { Vaults } from '../vaults/vaults';
import { Artists } from '../artists/artists';
import { Shop } from '../shop/shop';

@Component({
	templateUrl: 'tabs.html'
})
export class Tabs {
	tab1Root = News;
	tab2Root = Events;
	tab3Root = Vaults;
	tab4Root = Artists;
	tab5Root = Shop;
	
	constructor() {}
}