import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

import { ApiProvider } from './../../providers/api/api';

@Component({
	selector: 'page-artist-filter',
	templateUrl: 'artist-filter.html'
})
export class ArtistFilter {
	constructor(public viewCtrl: ViewController, public navCtrl: NavController, public navParams: NavParams, public api: ApiProvider) {
	
	};
	
	dismiss() {
		this.viewCtrl.dismiss();
	}
}