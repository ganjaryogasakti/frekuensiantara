import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Events } from 'ionic-angular';

import { Checkout } from '.././checkout/checkout';

import { CartProvider } from './../../providers/cart/cart';

@Component({
	selector: 'page-cart',
	templateUrl: 'cart.html'
})
export class Cart {
	loading: any;
	pageLoaded: boolean = false;
	cartItems: any[] = [];
	totalAmount: number = 0;
	isEmptyCart: boolean = true;
	
	// Constructor
	constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, private cartService: CartProvider, public events: Events) {
		this.loading = this.loadingCtrl.create();
		this.loading.present();
		this.loadCartItems();
	};
	
	// Enter Page
	ionViewWillEnter() {
		this.events.publish('tabs:show', false);
	};
	
	// Leave Page
	ionViewWillLeave() {
		this.events.publish('tabs:show', true);
	};
	
	// Load Cart
	loadCartItems() {
		this.cartService
		.getCartItems()
		.then(data => {
			this.cartItems = data;
			
			if (this.cartItems.length > 0) {
				this.cartItems.forEach((v, indx) => {
					this.totalAmount += parseInt(v.subtotal);
				});
				this.isEmptyCart = false;
			} else {
				this.totalAmount = 0;
				this.isEmptyCart = true;
			};
			
			this.loading.dismiss();
			this.pageLoaded = true;
		}).catch(err => {});
	};
	
	// Remove Item
	removeItem(item) {
		this.loading = this.loadingCtrl.create();
		this.loading.present();
		
		this.cartService.removeFromCart(item).then(() => {
			this.loadCartItems();
		});
	};
	
	// Checkout
	checkOut() {
		this.navCtrl.push(Checkout);
	};
};