import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';

import { ApiProvider } from './../../providers/api/api';

@Component({
	selector: 'page-about',
	templateUrl: 'about.html'
})
export class About {	
	result: {};
	loading: any;
	pageLoaded: boolean = false;
	
	constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public api: ApiProvider) {
		this.loading = this.loadingCtrl.create();
		this.loading.present();
		this.api.getAbout().then(result => {
			if (result != null) {
				this.result = result;
			} else {
				let toast = this.toastCtrl.create({
					message: 'The page can\'t be load or something\'s wrong.',
					duration: 3000,
					position: 'bottom'
				});
				toast.present();
			}
			this.loading.dismiss();
			this.pageLoaded = true;
		});
	};
}