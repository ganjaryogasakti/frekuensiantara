import { Component, ElementRef } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, ModalController } from 'ionic-angular';

import { ApiProvider } from './../../providers/api/api';
import { Artist } from '../artist/artist';
import { ArtistFilter } from '../artist-filter/artist-filter';

@Component({
	selector: 'page-artists',
	templateUrl: 'artists.html'
})
export class Artists {
	artists: Array<any> = new Array<any>();
	loading:	 any;
	scrollerHandle: any;
	page: number = 1;
	pageLoaded: boolean = false;
	noMoreItemsAvailable: boolean = false;
	placeholder: any = '../../assets/imgs/placeholder-article.svg';
	
	constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public modalCtrl: ModalController, public api: ApiProvider, public el: ElementRef) {
		this.loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: `<div class="fa-loading"><img src="../../assets/imgs/loader.svg"></div>`
		});
		this.loading.present();
		this.loadArtists();
	};
	
	// Enter Page
	ionViewWillEnter() {
		this.scrollerHandle = this.el.nativeElement.getElementsByClassName('scroll-content')[0];
	};
	
	// Load Posts
	loadArtists(infiniteScroll?) {
		this.api.getArtists(this.page).then(data => {
			if(data) {
				this.page++;
				this.artists = this.artists.concat(data);
				if(infiniteScroll) {
					infiniteScroll.complete();
				};
				if(this.artists.length < 12) {
					this.noMoreItemsAvailable = true;
				};
				if(!this.pageLoaded) {
					this.loading.dismiss();
					this.pageLoaded = true;
				};
			} else {
				let toast = this.toastCtrl.create({
					message: 'You have already reach the last post!',
					duration: 3000,
					position: 'bottom'
				});
				toast.present();
				infiniteScroll.complete();
				infiniteScroll.enable(false);
				this.noMoreItemsAvailable = true;
			};
		});
	};
	
	// Infinite Scroll
	doInfinite(infiniteScroll) {
		this.loadArtists(infiniteScroll);
	};
	
	// Go To Post
	artistTapped(event, item) {
		this.navCtrl.push(Artist, {
			id: item
		});
	};
	
	// Open Filter
	openFilter() {
		const modal = this.modalCtrl.create(ArtistFilter);
		modal.present();
	};
}