import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

import { ApiProvider } from './../../providers/api/api';

@Component({
	selector: 'page-projekt-filter',
	templateUrl: 'projekt-filter.html'
})
export class ProjektFilter {
	constructor(public viewCtrl: ViewController, public navCtrl: NavController, public navParams: NavParams, public api: ApiProvider) {
	
	};
	
	dismiss() {
		this.viewCtrl.dismiss();
	}
}