import { Component, ElementRef, Renderer } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, Events } from 'ionic-angular';

import { ApiProvider } from './../../providers/api/api';
import { GoogleMaps, GoogleMap, GoogleMapsEvent, GoogleMapOptions, Marker, Environment } from "@ionic-native/google-maps";

@Component({
	selector: 'page-event',
	templateUrl: 'event.html'
})
export class EventPost {
	id: number;
	event: {};
	loading:	 any;
	pageLoaded: boolean = false;
	scrollerHandle: any;
	scrollTop: any;
	header: any;
	translateAmt: any;
	map: GoogleMap;
	placeholder: any = '../../assets/imgs/placeholder-article.svg';
	
	constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public api: ApiProvider, public element: ElementRef, public renderer: Renderer, public events: Events) {
		this.id = navParams.get('id');
		this.loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: `<div class="fa-loading"><img src="../../assets/imgs/loader.svg"></div>`
		});
		this.loading.present();
		
		this.api.getEvent(this.id).then(data => {
			if(data) {
				this.event = data;
			} else {
				let toast = this.toastCtrl.create({
					message: 'The event is failed to load.',
					duration: 3000,
					position: 'bottom'
				});
				toast.present();
			};
			this.loading.dismiss();
			this.pageLoaded = true;
		});
	};
	
	// Enter Page
	ionViewWillEnter() {
		this.events.publish('tabs:show', false);
		this.scrollerHandle = this.element.nativeElement.getElementsByClassName('scroll-content')[0];
		this.header = this.scrollerHandle.getElementsByClassName('parallax-bg');
		
		this.scrollerHandle.addEventListener('scroll', () => {
			this.parallax();
		});
	};
	
	// Leave Page
	ionViewWillLeave() {
		this.events.publish('tabs:show', true);
	};
	
	// On Load Page
	ionViewDidLoad() {
		//this.loadMap();
	};

	
	// Parallax
	parallax() {
		this.scrollTop = this.scrollerHandle.scrollTop;
		
	    if(this.scrollTop >= 0){
	    	this.translateAmt = this.scrollTop / 2;
	    } else {
	    	this.translateAmt = 0;
	    }
		
		this.renderer.setElementStyle(this.header[0], 'webkitTransform', 'translate3d(0,'+this.translateAmt+'px,0)');
	};
	
	// Load Map
	loadMap() {
		Environment.setEnv({
			'API_KEY_FOR_BROWSER_RELEASE': '(your api key for `https://`)',
			'API_KEY_FOR_BROWSER_DEBUG': '(your api key for `http://`)'
		});
		
		let mapOptions: GoogleMapOptions = {
			camera: {
				target: {
					lat: 43.0741904,
					lng: -89.3809802
				},
				zoom: 18,
				tilt: 30
			}
		};
				
		this.map = GoogleMaps.create('map_canvas', mapOptions);
				
		let marker: Marker = this.map.addMarkerSync({
			title: 'Ionic',
			icon: 'blue',
			animation: 'DROP',
			position: {
				lat: 43.0741904,
				lng: -89.3809802
			}
		});
		
		marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
			alert('clicked');
		});
	};
}
