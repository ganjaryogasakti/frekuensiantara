import { Component, ElementRef } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';

import { ApiProvider } from './../../providers/api/api';

import { Product } from '.././product/product';
import { Cart } from '.././cart/cart';

import { CartProvider } from './../../providers/cart/cart';

@Component({
	selector: 'page-shop',
	templateUrl: 'shop.html'
})
export class Shop {
	cartTotal: number = 0;
	id: number;
	loading: any;
	scrollerHandle: any;
	pageLoaded: boolean = false;
	title: any;
	categories: any[];
	products: any[];
	page: number = 1;
	noMoreItemsAvailable: boolean = false;
	placeholder: any = '../../assets/imgs/placeholder-article.svg';
	
	// Constructor
	constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public cartService: CartProvider, public api: ApiProvider, public el: ElementRef) {
		this.id = navParams.get('id');
		this.loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: `<div class="fa-loading"><img src="../../assets/imgs/loader.svg"></div>`
		});
		this.loading.present();
		this.loadShop();
	};
	
	// Enter Page
	ionViewWillEnter() {
		this.scrollerHandle = this.el.nativeElement.getElementsByClassName('scroll-content')[0];
		this.getCartTotal();
	};
	
	// Load Shop
	loadShop() {
		this.api.getShop().then(data => {
			this.page++;
			this.title		= data['title'];
			this.categories	= data['categories'];
			this.products	= data['products'];
			if(data['products'].length < 12) {
				this.noMoreItemsAvailable = true;
			};
			if(!this.pageLoaded) {
				this.loading.dismiss();
				this.pageLoaded = true;
			};
		});
	};
	
	// Load Products
	loadProducts(infiniteScroll?) {
		this.api.getProducts(this.page, this.id).then(data => {
			if(data) {
				this.page++;
				this.products = this.products.concat(data);
				if(infiniteScroll) {
					infiniteScroll.complete();
				};
				if(this.products.length < 12) {
					this.noMoreItemsAvailable = true;
				};
				if(!this.pageLoaded) {
					this.loading.dismiss();
					this.pageLoaded = true;
				};
			} else {
				let toast = this.toastCtrl.create({
					message: 'You have already reach the last products!',
					duration: 3000,
					position: 'bottom'
				});
				toast.present();
				infiniteScroll.complete();
				infiniteScroll.enable(false);
				this.noMoreItemsAvailable = true;
			};
		});
	};
	
	// Category Select
	categorySelect(catID) {
		this.title		= [];
		this.categories	= [];
		this.products	= [];
		this.page		= 1;
		
		this.loading = this.loadingCtrl.create();
		this.loading.present();
		
		this.loadShop();
		this.loadProducts(catID);
	};
	
	// Infinite Scroll
	doInfinite(infiniteScroll) {
		this.loadProducts(infiniteScroll);
	};
	
	// Product Tap
	productTapped(event, id) {
		this.navCtrl.push(Product, {
			id: id
		});
	};
	
	// Open Cart
	openCart() {
		this.navCtrl.push(Cart);
	};
	
	// Cart Total
	getCartTotal() {
		this.cartService
		.getCartItems()
		.then(data => {
			if(data.length) {
				let qts = 0;
				for (var i = 0; i < data.length; i++) {
					qts += parseInt(data[i].qty);
				};
				this.cartTotal = qts;
			} else {
				this.cartTotal = 0;
			};
		}).catch(err => {});
	};
};