import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { CartProvider } from './../../providers/cart/cart';

@Component({
	selector: 'page-checkout',
	templateUrl: 'checkout.html'
})
export class Checkout {
	cartTotal: number = 0;
	selectedItem: any;
	
	// Constructor
	constructor(public navCtrl: NavController, public navParams: NavParams, public cartService: CartProvider) { };
	
	
};