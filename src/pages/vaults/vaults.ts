import { Component, ElementRef } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, ModalController } from 'ionic-angular';

import { ApiProvider } from './../../providers/api/api';
import { Vault } from '../vault/vault';

@Component({
	selector: 'page-vaults',
	templateUrl: 'vaults.html'
})
export class Vaults {
	now: any;
	interval: any;
	vaults: Array<any> = new Array<any>();
	loading:	 any;
	scrollerHandle: any;
	page: number = 1;
	pageLoaded: boolean = false;
	noMoreItemsAvailable: boolean = false;
	placeholder: any = '../../assets/imgs/placeholder-article.svg';
	
	constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public modalCtrl: ModalController, public api: ApiProvider, public el: ElementRef) {
		this.loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: `<div class="fa-loading"><img src="../../assets/imgs/loader.svg"></div>`
		});
		this.loading.present();
		this.loadVaults();
		
		this.interval = setInterval(() => {
			this.now = new Date().getTime();
		}, 1000);
	};
	
	// Enter Page
	ionViewWillEnter() {
		this.scrollerHandle = this.el.nativeElement.getElementsByClassName('scroll-content')[0];
	};
	
	// Load Posts
	loadVaults(infiniteScroll?) {
		this.api.getVaults(this.page).then(data => {
			if(data) {
				this.page++;
				this.vaults = this.vaults.concat(data);
				if(infiniteScroll) {
					infiniteScroll.complete();
				};
				if(this.vaults.length < 12) {
					this.noMoreItemsAvailable = true;
				};
				if(!this.pageLoaded) {
					this.loading.dismiss();
					this.pageLoaded = true;
				};
			} else {
				let toast = this.toastCtrl.create({
					message: 'You have already reach the last post!',
					duration: 3000,
					position: 'bottom'
				});
				toast.present();
				infiniteScroll.complete();
				infiniteScroll.enable(false);
				this.noMoreItemsAvailable = true;
			};
		});
	};
	
	// Infinite Scroll
	doInfinite(infiniteScroll) {
		this.loadVaults(infiniteScroll);
	};
	
	// Go To Post
	vaultTapped(event, item) {
		this.navCtrl.push(Vault, {
			id: item
		});
	};
	
	
	checkRelease(date) {
		var
		now		= new Date().getTime(),
		release	= new Date(date + ' 00:00:00').getTime(),
		expired	= Math.floor((now - release) / (1000 * 60 * 60 * 24));
		
		if (expired == 0) {
			return true;
		} else {
			return false;
		}
	};
}