import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, Events } from 'ionic-angular';

import { Cart } from '.././cart/cart';

import { ApiProvider } from './../../providers/api/api';
import { CartProvider } from './../../providers/cart/cart';

@Component({
	selector: 'page-product',
	templateUrl: 'product.html'
})
export class Product {
	cartTotal: number = 0;
	id: number;
	loading: any;
	pageLoaded: boolean = false;
	product: {};
	qty: number = 1;
	
	// Constructor
	constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public api: ApiProvider, private cartService: CartProvider, public events: Events) {
		this.id = navParams.get('id');
		this.loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: `<div class="fa-loading"><img src="../../assets/imgs/loader.svg"></div>`
		});
		this.loading.present();
		
		this.api.getProduct(this.id).then(data => {
			if(data) {
				this.product = data;
			} else {
				let toast = this.toastCtrl.create({
					message: 'The artist is failed to load.',
					duration: 3000,
					position: 'bottom'
				});
				toast.present();
			};
			this.loading.dismiss();
			this.pageLoaded = true;
		});
	};
	
	// Enter Page
	ionViewWillEnter() {
		this.events.publish('tabs:show', false);
		this.getCartTotal();
	};
	
	// Leave Page
	ionViewWillLeave() {
		this.events.publish('tabs:show', true);
	};
	
	// Add Quantity
	addQty() {
		this.qty += 1;
	};
	
	// Decrease Quantity
	decQty() {
		if(this.qty > 1) {
			this.qty -= 1;
		};
	};
	
	// Add to Cart
	addToCart(product) {
		let subtotal = this.qty * parseInt(product.price);
		
		let cartProduct = {
			product_id: product.id,
			name: product.title,
			thumb: product.image,
			price: product.price,
			qty: this.qty,
			subtotal: subtotal
		};
		
		this.cartService.addToCart(cartProduct).then((val) => {
			console.log(val);
			this.presentToast(cartProduct.name);
		});
	};
	  
	// Open Cart
	openCart() {
		this.navCtrl.push(Cart);
	};
	
	// Toastr
	presentToast(name) {
		let toast = this.toastCtrl.create({
			message: `${name} has been added to cart`,
			duration: 3000,
			position: 'bottom'
		});
		toast.present();
	};
	
	// Cart Total
	getCartTotal() {
		this.cartService
		.getCartItems()
		.then(data => {
			let qts = 0;
			if(data.length) {
				for (let i = 0; i < data.length; i++) {
					qts += parseInt(data[i].qty);
				};
			};
			this.cartTotal = qts;
		}).catch(err => {});
	};
};