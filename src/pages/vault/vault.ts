import { Component, ElementRef, Renderer } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, Events } from 'ionic-angular';

import { SocialSharing } from '@ionic-native/social-sharing';
import { ApiProvider } from './../../providers/api/api';
import { Comments } from '../comments/comments';
import { Post } from '../post/post';
import { Product } from '.././product/product';

@Component({
	selector: 'page-vault',
	templateUrl: 'vault.html'
})
export class Vault {
	id: number;
	vault: {};
	loading:	 any;
	pageLoaded: boolean = false;
	scrollerHandle: any;
	scrollTop: any;
	header: any;
	translateAmt: any;
	isBookmark: boolean = false;
	placeholder: any = '../../assets/imgs/placeholder-article.svg';
	related: any[];
	release: any;
	release_date: any;
	now: any;
	expired: any = 0;

	constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public api: ApiProvider, public element: ElementRef, public renderer: Renderer, private socialSharing: SocialSharing, public events: Events) {
		this.id = navParams.get('id');
		this.loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: `<div class="fa-loading"><img src="../../assets/imgs/loader.svg"></div>`
		});
		this.loading.present();

		this.api.getVault(this.id).then(data => {
			if(data) {
				this.vault = data;
				this.release_date = data['release_date'];
				this.now = new Date().getTime();
				this.release = new Date(this.release_date).getTime();
				this.expired = Math.floor((this.release - this.now) / (1000 * 60 * 60 * 24));
			} else {
				let toast = this.toastCtrl.create({
					message: 'The post is failed to load.',
					duration: 3000,
					position: 'bottom'
				});
				toast.present();
			};
			this.loading.dismiss();
			this.pageLoaded = true;
		});
	};
	
	// Enter Page
	ionViewWillEnter() {
		this.events.publish('tabs:show', false);
		this.scrollerHandle = this.element.nativeElement.getElementsByClassName('scroll-content')[0];
		this.header = this.scrollerHandle.getElementsByClassName('parallax-bg');

		this.scrollerHandle.addEventListener('scroll', () => {
			this.parallax();
		});
	};
	
	// Leave Page
	ionViewWillLeave() {
		this.events.publish('tabs:show', true);
	};

	// Parallax
	parallax() {
		this.scrollTop = this.scrollerHandle.scrollTop;

	    if(this.scrollTop >= 0){
	    	this.translateAmt = this.scrollTop / 2;
	    } else {
	    	this.translateAmt = 0;
	    }

		this.renderer.setElementStyle(this.header[0], 'webkitTransform', 'translate3d(0,'+this.translateAmt+'px,0)');
	};

	// Go To Post
	commentsTapped(event, item) {
		this.navCtrl.push(Comments, {
			id: item
		});
	};
	
	// Share Post
	sharePost(event, title, url) {
		this.socialSharing.share(title, '', '', url);
	};
	
	// Open Player
	openPlayer(event, code) {
		this.events.publish('player:toggle', true, code);
	};
	
	// Go To Post
	postTapped(event, post) {
		if(post.format != 'audio') {
			this.navCtrl.push(Post, {
				id: post.id
			});
		} else {
			this.events.publish('player:toggle', true, post.audio);
		};
	};
	
	// Product Tap
	productTapped(event, id) {
		this.navCtrl.push(Product, {
			id: id
		});
	};
}