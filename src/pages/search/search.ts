import { Component, ElementRef } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';

import { ApiProvider } from './../../providers/api/api';
import { Post } from '../post/post';

@Component({
	selector: 'page-search',
	templateUrl: 'search.html'
})
export class Search {
	keywords: any;
	posts: Array<any> = new Array<any>();
	loading:	 any;
	scrollerHandle: any;
	page: number = 1;
	pageLoaded: boolean = false;
	noMoreItemsAvailable: boolean = false;
	placeholder: any = '../../assets/imgs/placeholder-article.svg';

	constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public api: ApiProvider, public el: ElementRef) {
		this.keywords = navParams.get('key');
		this.loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: `<div class="fa-loading"><img src="../../assets/imgs/loader.svg"></div>`
		});
		this.loading.present();
		this.loadSearch();
	};

	// Enter Page
	ionViewWillEnter() {
		this.scrollerHandle = this.el.nativeElement.getElementsByClassName('scroll-content')[0];
	};
	
	// Load Posts
	loadSearch(infiniteScroll?) {
		this.api.getSearch(this.keywords, this.page).then(data => {
			if(data) {
				this.page++;
				this.posts = this.posts.concat(data);
				if(infiniteScroll) {
					infiniteScroll.complete();
				};
				if(!this.pageLoaded) {
					this.loading.dismiss();
					this.pageLoaded = true;
				};
			} else {
				let toast = this.toastCtrl.create({
					message: 'You have reached the last post!',
					duration: 3000,
					position: 'bottom'
				});
				toast.present();
				infiniteScroll.complete();
				infiniteScroll.enable(false);
				this.noMoreItemsAvailable = true;
			};
		});
	};
	
	// Infinite Scroll
	doInfinite(infiniteScroll) {
		this.loadSearch(infiniteScroll);
	};

	// Go To Post
	postTapped(event, item) {
		this.navCtrl.push(Post, {
			id: item
		});
	};
}
