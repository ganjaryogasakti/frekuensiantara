import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

const MARK_KEY = 'bookmarkPosts';

@Injectable()
export class ApiProvider {
	url: string = 'https://temp.frekuensiantara.com/wp-json/';

	constructor(public http: HttpClient, public storage: Storage) { };

	// Home
	getFeatures() {
		return new Promise(resolve => {
			this.http.get(this.url+'fa/v1/home')
			.subscribe(data => {
				resolve(data);
			}, err => {
				resolve(null);
			});
		});
	};


	// Posts
	getPosts(page:number = 1, exclude:string = '0') {
		return new Promise(resolve => {
			this.http.get(this.url+'wp/v2/posts?page='+page+'&exclude='+exclude)
			.subscribe(data => {
				resolve(data);
			}, err => {
				resolve(null);
			});
		});
	};


	// Post
	getPost(id) {
		return new Promise(resolve => {
			this.http.get(this.url+'wp/v2/posts/'+id)
			.subscribe(data => {
				resolve(data);
			}, err => {
				resolve(null);
			});
		});
	};
	
	
	// Search
	getSearch(keywords:string = '0', page:number = 1) {
		return new Promise(resolve => {
			this.http.get(this.url+'wp/v2/posts?search='+keywords+'&page='+page)
			.subscribe(data => {
				resolve(data);
			}, err => {
				resolve(null);
			});
		});
	};
	
	
	// Bookmarks
	getBookmarks() {
		let bookmarks = this.storage.get(MARK_KEY);
		if(bookmarks) {
			return bookmarks;
		} else {
			return null;
		}
	};
	
	
	// Bookmark
	getBookmark(post) {
		return this.getBookmarks().then(result => {
			if(result.length) {
				if (result.indexOf(post)) {
					return true;
				} else {
					return null;
				}
			} else {
				return null;
			}
		})
	};
	
	
	// Add Bookmark
	addBookmark(post) {
		return this.getBookmarks().then(result => {
			if (result.length) {
				if (!this.containsObject(post, result)) {
					result.push(post);
					return this.storage.set(MARK_KEY, result);
				};
			} else {
				return this.storage.set(MARK_KEY, [post]);
			};
		});
	};
	
	// Remove Bookmark
	removeBookmark(post) {
		return this.getBookmarks().then(result => {
			if (result) {
				var postIndex = result.indexOf(post);
				result.splice(postIndex, 1);
				return this.storage.set(MARK_KEY, result);
			}
		})
	};


	// Events
	getEvents(page:number = 1) {
		return new Promise(resolve => {
			this.http.get(this.url+'wp/v2/event?page='+page)
			.subscribe(data => {
				resolve(data);
			}, err => {
				resolve(null);
			});
		});
	};


	// Post
	getEvent(id) {
		return new Promise(resolve => {
			this.http.get(this.url+'wp/v2/event/'+id)
			.subscribe(data => {
				resolve(data);
			}, err => {
				resolve(null);
			});
		});
	};


	// Projekts
	getProjekts(page:number = 1) {
		return new Promise(resolve => {
			this.http.get(this.url+'wp/v2/projekt?page='+page)
			.subscribe(data => {
				resolve(data);
			}, err => {
				resolve(null);
			});
		});
	};


	// Projekt
	getProjekt(id) {
		return new Promise(resolve => {
			this.http.get(this.url+'wp/v2/projekt/'+id)
			.subscribe(data => {
				resolve(data);
			}, err => {
				resolve(null);
			});
		});
	};
	
	
	// Vaults
	getVaults(page:number = 1) {
		return new Promise(resolve => {
			this.http.get(this.url+'wp/v2/vault?page='+page)
			.subscribe(data => {
				resolve(data);
			}, err => {
				resolve(null);
			});
		});
	};


	// Vault
	getVault(id) {
		return new Promise(resolve => {
			this.http.get(this.url+'wp/v2/vault/'+id)
			.subscribe(data => {
				resolve(data);
			}, err => {
				resolve(null);
			});
		});
	};


	// Artists
	getArtists(page:number = 1) {
		return new Promise(resolve => {
			this.http.get(this.url+'wp/v2/artist?page='+page)
			.subscribe(data => {
				resolve(data);
			}, err => {
				resolve(null);
			});
		});
	};


	// Artist
	getArtist(id) {
		return new Promise(resolve => {
			this.http.get(this.url+'wp/v2/artist/'+id)
			.subscribe(data => {
				resolve(data);
			}, err => {
				resolve(null);
			});
		});
	};

	// Comments
	getComments(id, orderby:string = 'parent', order:string = 'asc', page:number = 1) {
		return new Promise(resolve => {
			this.http.get(this.url+'wp/v2/comments?post='+id+'&orderby='+orderby+'&order='+order+'&page='+page+'&per_page=20')
			.subscribe(data => {
				resolve(data);
			}, err => {
				resolve(null);
			});
		});
	}

	// Shop
	getShop() {
		return new Promise(resolve => {
			this.http.get(this.url+'fa/v1/shop')
			.subscribe(data => {
				resolve(data);
			}, err => {
				resolve(null);
			});
		});
	};


	// Products
	getProducts(page:number = 1, cat:number = 0) {
		var cat_query = (cat != 0) ? 'product_cat='+cat+'&' : '';
		return new Promise(resolve => {
			this.http.get(this.url+'wp/v2/product?'+cat_query+'page='+page)
			.subscribe(data => {
				resolve(data);
			}, err => {
				resolve(null);
			});
		});
	};


	// Product
	getProduct(id) {
		return new Promise(resolve => {
			this.http.get(this.url+'wp/v2/product/'+id)
			.subscribe(data => {
				resolve(data);
			}, err => {
				resolve(null);
			});
		});
	};
	
	// Contains Object Filter
	containsObject(obj, list): boolean {
		if (!list.length) {
			return false;
		};
	 
		if (obj == null) {
			return false;
		};
		
		var i;
		for (i = 0; i < list.length; i++) {
			if (list[i].id == obj.id) {
				return true;
			};
		};
		return false;
	};
	
	
	// About
	getAbout() {
		return new Promise(resolve => {
			this.http.get(this.url+'fa/v1/about')
			.subscribe(data => {
				resolve(data);
			}, err => {
				resolve(null);
			});
		});
	};
	
	
	// Single Page
	getPage(id) {
		return new Promise(resolve => {
			this.http.get(this.url+'wp/v2/pages/'+id)
			.subscribe(data => {
				resolve(data);
			}, err => {
				resolve(null);
			});
		});
	};
}
